﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SincronizacionOrdenes
{
    class Client
    {
        public class Customer
        {
            public int id { get; set; }
            public string email { get; set; }
            public string userName { get; set; }
            public string adminComment { get; set; }
            public bool isTaxExcept { get; set; }
            public bool active { get; set; }
            public int affiliateId { get; set; }
            public string vatNumber { get; set; }
            public DateTime createdOn { get; set; }
            public DateTime lastActivityDate { get; set; }
            public string lastIpAddress { get; set; }
            public string lastVisitPage { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string gender { get; set; }
            public DateTime? dateOfBirth { get; set; }
            public string companyName { get; set; }
            public string streetAddress { get; set; }
            public string streetAddress2 { get; set; }
            public string zipPostalCode { get; set; }
            public string city { get; set; }
            public string countryName { get; set; }
            public string stateName { get; set; }
            public string phoneNumber { get; set; }
            public string NroDocument { get; set; }
            public string rolName { get; set; }
        }
    }
}
