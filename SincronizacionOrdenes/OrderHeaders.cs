﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SincronizacionOrdenes
{
    class OrderHeaders
    {
        public class Headers
        {
            public int id { get; set; }
            public int storeId { get; set; }
            public int customerId { get; set; }
            public int billingAddressId { get; set; }
            public int? shippingAddressId { get; set; }
            public int? pickupAddressId { get; set; }
            public bool pickupInStore { get; set; }
            public int orderStatusId { get; set; }
            public int shippingStatusId { get; set; }
            public int paymentStatusId { get; set; }
            public string paymentMethodSystemName { get; set; }
            public string customerCurrencyCode { get; set; }
            public decimal currencyRate { get; set; }
            public int customerTaxDisplayTypeId { get; set; }
            public string vatNumber { get; set; }
            public decimal orderSubtotalInclTax { get; set; }
            public decimal orderSubTotalDiscountInclTax { get; set; }
            public decimal orderShippingInclTax { get; set; }
            public decimal orderShippingExclTax { get; set; }
            public decimal paymentMethodAdditionalFeeInclTax { get; set; }
            public decimal paymentMethodAdditionalFeeExclTax { get; set; }
            public string taxRates { get; set; }
            public decimal orderTax { get; set; }
            public decimal orderDiscount { get; set; }
            public decimal orderTotal { get; set; }
            public decimal refundedAmount { get; set; }
            public bool rewardPointsWereAdded { get; set; }
            public string checkoutAttributeDescription { get; set; }
            public string checkoutattributesXml { get; set; }
            public int customerLanguageId { get; set; }
            public int affiliatedId { get; set; }
            public string customerIp { get; set; }
            public bool allowStoringCreditCardNumber { get; set; }
            public string cardType { get; set; }
            public string cardName { get; set; }
            public string cardNumber { get; set; }
            public string maskedCreditCardNumber { get; set; }
            public string cardCvv2 { get; set; }
            public string cardExpirationMonth { get; set; }
            public string cardExpirationYear { get; set; }
            public string authorizationTransactionId { get; set; }
            public string authorizationTransactionCode { get; set; }
            public string authorizationTransactionResult { get; set; }
            public string captureTransactionId { get; set; }
            public string captureTransactionResult { get; set; }
            public string subscriptionTransactionId { get; set; }
            public DateTime? paidDateUtc { get; set; }
            public string shippingMethod { get; set; }
            public string shippingRateComputationMethodSystemName { get; set; }
            public string customValuesXml { get; set; }
            public bool deleted { get; set; }
            public DateTime createdOnUtc { get; set; }
        }
    }
}
