﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SincronizacionOrdenes
{
    class OrderAddress
    {
        public class Address
        {
            public int orderId { get; set; }
            public int id { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string email { get; set; }
            public string country { get; set; }
            public string state { get; set; }
            public string city { get; set; }
            public string address1 { get; set; }
            public string address2 { get; set; }
            public string zipPostalCode { get; set; }
            public string phoneNumber { get; set; }
            public string nroDocument { get; set; }
        }
    }
}
