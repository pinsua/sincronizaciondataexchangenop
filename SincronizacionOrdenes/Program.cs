﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;

namespace SincronizacionOrdenes
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Establecemos la cultura para solucionar el tema de los formatos.
            Thread.CurrentThread.CurrentCulture = new CultureInfo("fr-FR");

            string _pathTxt = ConfigurationManager.AppSettings["PathTxt"];
            string _userNop = ConfigurationManager.AppSettings["UserNop"];
            string _passNop = ConfigurationManager.AppSettings["PassNop"];
            string _urlNop = ConfigurationManager.AppSettings["UrlNop"];
            int _cantMinimaOrden = Convert.ToInt32(ConfigurationManager.AppSettings["CantMinimaOrden"]);
            int _cantMaximaOrden = Convert.ToInt32(ConfigurationManager.AppSettings["CantMaximaOrden"]);
            string _nroClienteFinal = ConfigurationManager.AppSettings["NroClinteFinal"];
            //Envio mail.
            string _destinatarioEmail = ConfigurationManager.AppSettings["DestinatarioEmail"];
            string _destinatarioCopia = ConfigurationManager.AppSettings["DestinatarioCopia"];
            string _direccionEnvio = ConfigurationManager.AppSettings["DireccionEnvio"];
            string _nicEnvio = ConfigurationManager.AppSettings["NicEnvio"];
            string _asuntoEnvio = ConfigurationManager.AppSettings["AsuntoEnvio"];
            string _passwordDireccionEnvio = ConfigurationManager.AppSettings["PasswordDireccionEnvio"];
            string _puertoSmtp = ConfigurationManager.AppSettings["PuertoSmtp"];
            string _servidorSmtp = ConfigurationManager.AppSettings["ServidorSmtp"];
            string _tipoDoc = ConfigurationManager.AppSettings["TipoDoc"];
            string _serie = ConfigurationManager.AppSettings["Serie"];
            //DateExchange
            string _pathDE = ConfigurationManager.AppSettings["PathDE"];
            string _procesoDE = ConfigurationManager.AppSettings["ProcesoDE"];
            //Conexion sql
            string _sqlConnection = ConfigurationManager.ConnectionStrings["mng"].ConnectionString;

            List<OrderHeaders.Headers> _lstHeaders = new List<OrderHeaders.Headers>();

            try
            {
                //Recuperamos las ordenes.
                using (NopService.NopServiceClient _ws = new NopService.NopServiceClient())
                {
                    _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(_urlNop);

                    string _jsHeaders = _ws.SearchOrders(_userNop, _passNop, 10, null, null);
                    _lstHeaders = new JavaScriptSerializer().Deserialize<List<OrderHeaders.Headers>>(_jsHeaders);                    
                }

                //Recorro la lista
                foreach(OrderHeaders.Headers _oh in _lstHeaders)
                {
                    List<OrderDetail.Detail> _items = new List<OrderDetail.Detail>();
                    OrderAddress.Address _shippingAddress = new OrderAddress.Address();
                    Client.Customer _customer = new Client.Customer();
                    //Recupero los items de la orden.
                    using (NopService.NopServiceClient _wsItems = new NopService.NopServiceClient())
                    {
                        _wsItems.Endpoint.Address = new System.ServiceModel.EndpointAddress(_urlNop);

                        string _jsItems = _wsItems.GetOrderItem(_userNop, _passNop, _oh.id);
                        _items = new JavaScriptSerializer().Deserialize<List<OrderDetail.Detail>>(_jsItems);
                        //Recupero la direccion de envio.                        
                        if (_oh.pickupInStore)
                        {
                            string _jsPickup = _wsItems.GetPickupAddress(_userNop, _passNop, _oh.id);
                            _shippingAddress = new JavaScriptSerializer().Deserialize<OrderAddress.Address>(_jsPickup);
                        }
                        else
                        {
                            string _jsAddress = _wsItems.GetShippingAddress(_userNop, _passNop, _oh.id);
                            _shippingAddress = new JavaScriptSerializer().Deserialize<OrderAddress.Address>(_jsAddress);
                        }
                        //Recupero los datos del cliente.
                        string _custom = _wsItems.GetCustomer(_userNop, _passNop, _oh.customerId);
                        _customer = new JavaScriptSerializer().Deserialize<Client.Customer>(_custom);
                    }

                    string _fileName = "order" + _oh.id.ToString() + ".txt";
                    string _pathFile = _pathTxt + _fileName;
                    string _nroPedido = "E" + _oh.id.ToString().PadLeft(8, '0');

                    string _fecha = _oh.createdOnUtc.Day.ToString().PadLeft(2, '0') + "/" + _oh.createdOnUtc.Month.ToString().PadLeft(2, '0') + "/" + _oh.createdOnUtc.Year.ToString();
                    string _nroCliente = "";
                    if (String.IsNullOrEmpty(_customer.NroDocument))
                        _nroCliente = _nroClienteFinal;
                    else
                        _nroCliente = _customer.NroDocument;

                    List<String> _lines = new List<string>();
                    foreach(OrderDetail.Detail _it in _items)
                    {
                        //string ln = _tipoDoc + ";" + _serie + ";" + _nroPedido + ";" + _fecha + ";" + _nroCliente + ";" + _it.manufacturerPartNumber + ";" + _it.productName + ";" + _it.quantity.ToString() + ";" + _it.priceInclTax.ToString() + ";A1;" + "9;" +_shippingAddress.address1;
                        string ln = _tipoDoc + ";" + _serie + ";" + _nroPedido + ";" + _fecha + ";" + _nroCliente + ";" + _it.manufacturerPartNumber + ";" + _it.productName + ";" + _it.quantity.ToString() + ";" + _it.unitPriceExclTax.ToString() + ";A1;" + "9;" + _shippingAddress.address1;
                        _lines.Add(ln);
                    }

                    //Escribimos el archivo Txt.
                    System.IO.File.WriteAllLines(_pathFile, _lines);

                    System.Diagnostics.Process _dataExchange = new System.Diagnostics.Process();
                    _dataExchange.StartInfo.FileName = _pathDE;
                    _dataExchange.StartInfo.Arguments = _procesoDE + " " + _pathFile;

                    _dataExchange.Start();
                    _dataExchange.WaitForExit();
                    //dormimos
                    System.Threading.Thread.Sleep(30000);
                    //Luego revisar si existe el archivo, si no existe cambiar el estado de la orden.
                    if (PedVentaCab.ExistePedido(_nroPedido, _sqlConnection))
                    {
                        //Actualizo la orden en el ecommerce.
                        //Recupero los items de la orden.
                        using (NopService.NopServiceClient _wsItems = new NopService.NopServiceClient())
                        {
                            _wsItems.Endpoint.Address = new System.ServiceModel.EndpointAddress(_urlNop);

                            _wsItems.SetOrderStatusProcess(_oh.id, _userNop, _passNop);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                string _body = "Se produjo el siguiente error: " + ex.Message + ". El proceso de Sincronizacion de Ordenes no termino correctamente. Por favor reviselo.";

                LogFile.ErrorLog(LogFile.CreatePath(), _body);

                Email.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio, _asuntoEnvio, _body, true,
                    _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);
            }
            
        }
    }
}
