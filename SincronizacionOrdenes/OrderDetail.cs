﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SincronizacionOrdenes
{
    class OrderDetail
    {
        public class Detail
        {
            public int id { get; set; }
            public int orderId { get; set; }
            public int productId { get; set; }
            public string sku { get; set; }
            public string manufacturerPartNumber { get; set; }
            public string productName { get; set; }
            public int quantity { get; set; }
            public decimal unitPriceInclTax { get; set; }
            public decimal unitPriceExclTax { get; set; }
            public decimal priceInclTax { get; set; }
            public decimal priceExclTax { get; set; }
            public decimal discountAmountInclTax { get; set; }
            public decimal discountAmountExclTax { get; set; }
            public decimal originalProductCost { get; set; }
            public string attributeDescription { get; set; }
            public string attributesXml { get; set; }
            public int downloadCount { get; set; }
            public bool isDownloadActivated { get; set; }
            public int? licenseDownloadId { get; set; }
            public decimal? itemWeight { get; set; }
            public DateTime? rentalStartDateUtc { get; set; }
            public DateTime? rentalEndDateUtc { get; set; }
        }
    }
}
