﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SincronizacionOrdenes
{
    class PedVentaCab
    {
        public static bool ExistePedido(string _pedidoWeb, string _connectionSql)
        {
            string _sql = "select COUNT(*) from PEDVENTACAB where SUPEDIDO = @pedido";

            bool _rta = false;

            using (SqlConnection _con = new SqlConnection(_connectionSql))
            {
                _con.Open();

                using (SqlCommand _cmd = new SqlCommand())
                {
                    _cmd.Connection = _con;
                    _cmd.CommandType = System.Data.CommandType.Text;
                    _cmd.CommandText = _sql;

                    _cmd.Parameters.AddWithValue("@pedido", _pedidoWeb);

                    object _reader = _cmd.ExecuteScalar();
                    
                    try
                    {
                        int _cantidad = Convert.ToInt32(_reader);

                        if (_cantidad > 0)
                            _rta = true;
                        else
                            _rta = false;
                    }
                    catch
                    {
                        _rta = false;
                    }
                }
            }

            return _rta;
        }
    }
}
