﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;

namespace SincronizacionUpdatePictures
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            //Leemos los datos del archivo de configuracion.
            string _pathImagenes = ConfigurationManager.AppSettings["DirectorioImagenes"];
            string _userNop = ConfigurationManager.AppSettings["UserNop"];
            string _passNop = ConfigurationManager.AppSettings["PassNop"];
            string _urlNop = ConfigurationManager.AppSettings["UrlNop"];
            //Envio mail.
            string _destinatarioEmail = ConfigurationManager.AppSettings["DestinatarioEmail"];
            string _destinatarioCopia = ConfigurationManager.AppSettings["DestinatarioCopia"];
            string _direccionEnvio = ConfigurationManager.AppSettings["DireccionEnvio"];
            string _nicEnvio = ConfigurationManager.AppSettings["NicEnvio"];
            string _asuntoEnvio = ConfigurationManager.AppSettings["AsuntoEnvio"];
            string _passwordDireccionEnvio = ConfigurationManager.AppSettings["PasswordDireccionEnvio"];
            string _puertoSmtp = ConfigurationManager.AppSettings["PuertoSmtp"];
            string _servidorSmtp = ConfigurationManager.AppSettings["ServidorSmtp"];

            try
            {
                if (Directory.Exists(_pathImagenes))
                {
                    string[] images = Directory.GetFiles(_pathImagenes);
                    foreach (var imagen in images)
                    {
                        try
                        {
                            string _pictureName = System.IO.Path.GetFileName(imagen);
                            string[] _imageData = _pictureName.Split('-');
                            string _sku = _imageData[0].ToString();
                            LogFile.ErrorLog(LogFile.CreatePath(), "Imagen:" + _sku);
                            using (_nopService.NopServiceClient _ws = new _nopService.NopServiceClient())
                            {
                                _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(_urlNop);

                                string _wsRta = _ws.UpdateProductPicture(_userNop, _passNop, _sku);

                                LogFile.ErrorLog(LogFile.CreatePath(), "Sku: " + _sku + ". Respuesta: " + _wsRta);
                            }
                        }
                        catch(Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("El directorio: " + _pathImagenes + " no exite por favor revise el path.");
                }
            }
            catch(Exception ex)
            {
                string _body = "Se produjo el siguiente error: " + ex.Message + ". El proceso de Sincronizacion de Imagenes no termino correctamente. Por favor reviselo.";

                LogFile.ErrorLog(LogFile.CreatePath(), _body);

                Email.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio, _asuntoEnvio, _body, true,
                    _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);
            }
        }
    }
}
