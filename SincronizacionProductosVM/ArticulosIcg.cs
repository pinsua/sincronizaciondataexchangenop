﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SincronizacionProductosVM
{
    class ArticulosIcg
    {
        public string _sku { get; set; }
        public int _codigoArticulo { get; set; }
        public string _descatalogado { get; set; }
        public string _descripcion { get; set; }
        public string _refProveedor { get; set; }
        public DateTime _fechaModificado { get; set; }
        public bool _visibleWeb { get; set; }
        public decimal _peso { get; set; }
        public int _tipoImpuesto { get; set; }
        public decimal _ultimoCoste { get; set; }
        public string _departamento { get; set; }
        public string _seccion { get; set; }
        public string _familia { get; set; }
        public string _subfamilia { get; set; }
        public string _codBarras { get; set; }
        public string _codBarras2 { get; set; }
        public string _codBarras3 { get; set; }
        public string _talla { get; set; }
        public string _color { get; set; }
        public string _marca { get; set; }
        public string _linea { get; set; }
    }
}
