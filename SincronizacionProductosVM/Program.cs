﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;

namespace SincronizacionProductosVM
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());

            string _archivoTxt = ConfigurationManager.AppSettings["ArchivoTxt"];
            string _userNop = ConfigurationManager.AppSettings["UserNop"];
            string _passNop = ConfigurationManager.AppSettings["PassNop"];
            string _urlNop = ConfigurationManager.AppSettings["UrlNop"];
            int _cantMinimaOrden = Convert.ToInt32(ConfigurationManager.AppSettings["CantMinimaOrden"]);
            int _cantMaximaOrden = Convert.ToInt32(ConfigurationManager.AppSettings["CantMaximaOrden"]);
            //Envio mail.
            string _destinatarioEmail = ConfigurationManager.AppSettings["DestinatarioEmail"];
            string _destinatarioCopia = ConfigurationManager.AppSettings["DestinatarioCopia"];
            string _direccionEnvio = ConfigurationManager.AppSettings["DireccionEnvio"];
            string _nicEnvio = ConfigurationManager.AppSettings["NicEnvio"];
            string _asuntoEnvio = ConfigurationManager.AppSettings["AsuntoEnvio"];
            string _passwordDireccionEnvio = ConfigurationManager.AppSettings["PasswordDireccionEnvio"];
            string _puertoSmtp = ConfigurationManager.AppSettings["PuertoSmtp"];
            string _servidorSmtp = ConfigurationManager.AppSettings["ServidorSmtp"];

            if (System.IO.File.Exists(_archivoTxt))
            {
                //Instancio la lista de articulos.
                List<ArticulosIcg> _lstArticulos = new List<ArticulosIcg>();
                try
                {
                    //Leo el archivo.
                    using (StreamReader sr = new StreamReader(_archivoTxt, false))
                    {
                        string line;
                        //ARTICULOS.CODARTICULO, ARTICULOS.DESCRIPCION, DEPARTAMENTO.DESCRIPCION AS DEPARTAMENTO, SECCIONES.DESCRIPCION AS SECCION, 
                        //FAMILIAS.DESCRIPCION AS FAMILIA, SUBFAMILIAS.DESCRIPCION AS SUBFAMILIA, ARTICULOS.REFPROVEEDOR, ARTICULOS.DESCATALOGADO, 
                        //ARTICULOS.VISIBLEWEB, ARTICULOSLIN.ULTIMOCOSTE, ARTICULOSLIN.CODBARRAS, ARTICULOSLIN.CODBARRAS2, ARTICULOSLIN.CODBARRAS3, 
                        //ARTICULOS.TIPOIMPUESTO, ARTICULOSLIN.PESO, ARTICULOS.FECHAMODIFICADO, ARTICULOSLIN.TALLA, ARTICULOSLIN.COLOR, Marca, Linea
                        while ((line = sr.ReadLine()) != null)
                        {
                            //separo todo por el ;
                            string[] _Texto = line.Split(';');
                            //fecha
                            string _fecha = _Texto[15].Split('/')[1] + "/" + _Texto[15].Split('/')[0] + "/" + _Texto[15].Split('/')[2];
                            //Instancio la clase y la cargo.
                            ArticulosIcg _cls = new ArticulosIcg();
                            _cls._sku = _Texto[10];
                            _cls._codigoArticulo = Convert.ToInt32(_Texto[0]);
                            _cls._descatalogado = _Texto[7];
                            _cls._descripcion = _Texto[1];
                            _cls._refProveedor = _Texto[6];
                            _cls._fechaModificado = Convert.ToDateTime(_Texto[15].ToString());

                            _cls._visibleWeb = _Texto[8].ToString().ToUpper() == "F" ? false : true;
                            _cls._peso = Convert.ToDecimal(_Texto[14], System.Globalization.CultureInfo.InvariantCulture);
                            _cls._tipoImpuesto = Convert.ToInt32(_Texto[13]);
                            _cls._ultimoCoste = Convert.ToDecimal(_Texto[9], System.Globalization.CultureInfo.InvariantCulture);
                            _cls._codBarras2 = _Texto[11];
                            _cls._codBarras3 = _Texto[12];
                            _cls._departamento = _Texto[2];
                            _cls._familia = _Texto[4];
                            _cls._seccion = _Texto[3];
                            _cls._subfamilia = _Texto[5];

                            _cls._talla = _Texto[16];
                            _cls._color = _Texto[17];
                            _cls._marca = _Texto[18];
                            _cls._linea = _Texto[19];

                            //Lo sumo a la lista.
                            _lstArticulos.Add(_cls);
                        }
                    }

                    LogFile.ErrorLog(LogFile.CreatePath(), "El archivo se leyo correctamente.");

                    //Obtengo las familias.
                    List<string> _lstFamilia = _lstArticulos.Select(x => x._familia).Distinct().ToList();
                    //Obtengo las subfamilias.
                    var _lstSubFamilia = _lstArticulos.Select(x => new { x._familia, x._subfamilia }).Distinct().ToList();
                    //Obtengo las marcas.
                    List<string> _lstMarca = _lstArticulos.Select(x => x._marca).Distinct().ToList();
                    //Obtengo las lineas
                    var _lstLineas = _lstArticulos.Select(x => new { x._marca, x._linea}).Distinct().ToList();

                    //Procesamos la categorias.
                    using (NopService.NopServiceClient _ws = new NopService.NopServiceClient())
                    {
                        _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(_urlNop);

                        foreach (string _cat in _lstFamilia)
                        {
                            string _ok = _ws.SincroCategory(_userNop, _passNop, _cat, "");
                        }
                    }

                    //Procesamos la Subcategorias.
                    using (NopService.NopServiceClient _ws = new NopService.NopServiceClient())
                    {
                        _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(_urlNop);

                        foreach (var _cat in _lstSubFamilia)
                        {
                            string _ok;
                            if (!String.IsNullOrEmpty(_cat._subfamilia))
                                _ok = _ws.SincroCategory(_userNop, _passNop, _cat._subfamilia, _cat._familia);
                        }
                    }

                    //Procesmos las Marcas (Region).
                    using (
                        NopService.NopServiceClient _ws = new NopService.NopServiceClient())
                    {
                        _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(_urlNop);

                        foreach (string _mar in _lstMarca)
                        {
                            bool _ok;
                            if (!String.IsNullOrEmpty(_mar))
                                _ok = _ws.SincroSpecificationAttributes(_userNop, _passNop, "Región", _mar);
                        }
                    }

                    //Procesamos las Lineas (Colegio).
                    using (NopService.NopServiceClient _ws = new NopService.NopServiceClient())
                    {
                        _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(_urlNop);

                        foreach (var _lin in _lstLineas)
                        {
                            bool _ok;
                            if (!String.IsNullOrEmpty(_lin._linea))
                                _ok = _ws.SincroSpecificationAttributes(_userNop, _passNop, "Colegio", _lin._linea);
                        }
                    }
                    //Vemos si tenemos datos y los procesamos.
                    if (_lstArticulos.Count > 0)
                    {
                        foreach (ArticulosIcg _art in _lstArticulos)
                        {
                            if (!String.IsNullOrEmpty(_art._sku) && _art._codigoArticulo > 0 && _art._sku != "-")
                            {
                                using (NopService.NopServiceClient _ws = new NopService.NopServiceClient())
                                {
                                    _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(_urlNop);

                                    //Redondeamos el costo.
                                    decimal _costo = Math.Round(_art._ultimoCoste, 2);
                                    //Cargamos el Articulo.
                                    bool _rtaArticulo = _ws.AddProduct(_userNop,
                                        _passNop,
                                        _art._codigoArticulo.ToString(),
                                        _art._codBarras3,
                                        _art._descripcion,
                                        _art._descripcion,
                                        "",
                                        _art._familia,
                                        _art._subfamilia,
                                        _art._color,
                                        _art._talla,
                                        "0",
                                        "0",
                                        "0",
                                        _art._peso.ToString(),
                                        "0",
                                        "0",
                                        "0",
                                        "",
                                        "");
                                    //Vemos si se proceso el articulo.
                                    if (_rtaArticulo)
                                    {
                                        //Lo atamos a la categoria.
                                        if (!String.IsNullOrEmpty(_art._familia))
                                        {
                                            string _rtaCategoria = _ws.AddProductCategory(_userNop, _passNop, _art._familia, _art._codBarras3, "");
                                            if (!String.IsNullOrEmpty(_art._subfamilia))
                                            {
                                                string _rtaSubcategoria = _ws.AddProductCategory(_userNop, _passNop, _art._subfamilia, _art._codBarras3, _art._familia);
                                            }
                                        }
                                        //Lo atamos a las especificaciones.

                                    }
                                    //Comentamos todo por las dudas.
                                    //Analizar Rta.
                                    //if (_rtaArticulo)
                                    //{
                                    //    string _rtaPicture = _ws.AddProductPicture(_userNop, _passNop, _art._sku);
                                    //}
                                    //if (_rtaArticulo)
                                    //{
                                    //    if (!String.IsNullOrEmpty(_art._familia))
                                    //    {
                                    //        string _newFamilia = _art._familia;
                                    //        if (_art._familia == "GENERICO")
                                    //            _newFamilia = "Prendas Universales";
                                    //        if (_art._familia == "MOCHILAS" || _art._familia == "LUNCH BAG" || _art._familia == "CARTUCHERAS")
                                    //            _newFamilia = "Accesorios";
                                    //        if (_art._familia == "ZAPATOS")
                                    //            _newFamilia = "Calzados";
                                    //        string _rtaFamilia = _ws.AddProductCategory(_userNop, _passNop, _newFamilia, _art._codBarras3, "");
                                    //    }
                                    //}
                                }
                            }
                            else
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(), "Revisar los datos de SKU y CodArticulo. Articulo: " + _art._sku);
                            }
                        }
                    }
                    else
                    {
                        LogFile.ErrorLog(LogFile.CreatePath(), "No hay datos a sincronizar.");
                    }
                }
                catch (Exception ex)
                {
                    string _body = "Se produjo el siguiente error: " + ex.Message + ". El proceso de Sincronizacion de Articulos no termino correctamente. Por favor reviselo.";

                    LogFile.ErrorLog(LogFile.CreatePath(), _body);

                    Email.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio, _asuntoEnvio, _body, true,
                        _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);
                }
            }
        }
    }
}
