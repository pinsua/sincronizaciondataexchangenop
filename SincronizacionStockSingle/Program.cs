﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;

namespace SincronizacionStockSingle
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {            

            string _archivoTxt = ConfigurationManager.AppSettings["ArchivoTxt"];
            string _userNop = ConfigurationManager.AppSettings["UserNop"];
            string _passNop = ConfigurationManager.AppSettings["PassNop"];
            string _urlNop = ConfigurationManager.AppSettings["UrlNop"];
            //Envio mail.
            string _destinatarioEmail = ConfigurationManager.AppSettings["DestinatarioEmail"];
            string _destinatarioCopia = ConfigurationManager.AppSettings["DestinatarioCopia"];
            string _direccionEnvio = ConfigurationManager.AppSettings["DireccionEnvio"];
            string _nicEnvio = ConfigurationManager.AppSettings["NicEnvio"];
            string _asuntoEnvio = ConfigurationManager.AppSettings["AsuntoEnvio"];
            string _passwordDireccionEnvio = ConfigurationManager.AppSettings["PasswordDireccionEnvio"];
            string _puertoSmtp = ConfigurationManager.AppSettings["PuertoSmtp"];
            string _servidorSmtp = ConfigurationManager.AppSettings["ServidorSmtp"];
            
            if (System.IO.File.Exists(_archivoTxt))
            {
                //Instancio la lista de articulos.
                List<StockIcg> _lsStocks = new List<StockIcg>();
                try
                {
                    //Leo el archivo.
                    using (StreamReader sr = new StreamReader(_archivoTxt, false))
                    {
                        string line;

                        while ((line = sr.ReadLine()) != null)
                        {
                            //separo todo por el ;
                            string[] _Texto = line.Split(';');
                            //Instancio la clase y la cargo.
                            StockIcg _cls = new StockIcg();
                            _cls._sku = _Texto[0];
                            _cls._refProveedor = _Texto[1];
                            _cls._cantidad = Convert.ToDecimal(_Texto[2], System.Globalization.CultureInfo.InvariantCulture);
                            _cls._almacen = _Texto[3];
                            //Lo sumo a la lista.
                            _lsStocks.Add(_cls);
                        }
                    }

                    //MessageBox.Show("El archivo se proceso correctamente.", Application.ProductName);

                    foreach (StockIcg _st in _lsStocks)
                    {
                        if (!String.IsNullOrEmpty(_st._sku))
                        {
                            using (NopService.NopServiceClient _ws = new NopService.NopServiceClient())
                            {
                                _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(_urlNop);

                                bool _rta = _ws.UpdateStockSingle(_userNop, _passNop, _st._sku, Convert.ToInt32(_st._cantidad));

                                if (_rta)
                                    LogFile.ErrorLog(LogFile.CreatePath(), "El articulo: " + _st._sku + " se sincronizo correctamente.");
                                else
                                    LogFile.ErrorLog(LogFile.CreatePath(), "El articulo: " + _st._sku + " NO SE SINCRONIZO.");
                            }
                        }
                    }

                    LogFile.ErrorLog(LogFile.CreatePath(), "La Sincronizacion termino correctamente.");
                }
                catch (Exception ex)
                {
                    string _body = "Se produjo el siguiente error: " + ex.Message + ". El proceso de Sincronizacion de Stock no termino correctamente. Por favor reviselo.";

                    LogFile.ErrorLog(LogFile.CreatePath(), _body);

                    Email.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio, _asuntoEnvio, _body, true, 
                        _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);

                }
            }
        }
    }
}
