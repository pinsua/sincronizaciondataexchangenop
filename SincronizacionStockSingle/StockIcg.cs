﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SincronizacionStockSingle
{
    public class StockIcg
    {
        public string _sku { get; set; }
        public string _refProveedor { get; set; }
        public decimal _cantidad { get; set; }
        public string _almacen { get; set; }
    }
}
