﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SincronizacionPreciosVM
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string _archivoTxt = ConfigurationManager.AppSettings["ArchivoTxt"];
            string _userNop = ConfigurationManager.AppSettings["UserNop"];
            string _passNop = ConfigurationManager.AppSettings["PassNop"];
            string _urlNop = ConfigurationManager.AppSettings["UrlNop"];
            //Envio mail.
            string _destinatarioEmail = ConfigurationManager.AppSettings["DestinatarioEmail"];
            string _destinatarioCopia = ConfigurationManager.AppSettings["DestinatarioCopia"];
            string _direccionEnvio = ConfigurationManager.AppSettings["DireccionEnvio"];
            string _nicEnvio = ConfigurationManager.AppSettings["NicEnvio"];
            string _asuntoEnvio = ConfigurationManager.AppSettings["AsuntoEnvio"];
            string _passwordDireccionEnvio = ConfigurationManager.AppSettings["PasswordDireccionEnvio"];
            string _puertoSmtp = ConfigurationManager.AppSettings["PuertoSmtp"];
            string _servidorSmtp = ConfigurationManager.AppSettings["ServidorSmtp"];

            if (System.IO.File.Exists(_archivoTxt))
            {
                //Instancio la lista de articulos.
                List<PreciosIcg> _lsPrecios = new List<PreciosIcg>();
                try
                {
                    //Leo el archivo.
                    using (StreamReader sr = new StreamReader(_archivoTxt, false))
                    {
                        string line;

                        while ((line = sr.ReadLine()) != null)
                        {
                            //separo todo por el ;
                            string[] _Texto = line.Split(';');
                            int _idMng = Convert.ToInt32(_Texto[1]);
                            //Instancio la clase y la cargo.
                            PreciosIcg _cls = new PreciosIcg();
                            _cls._idTarifa = _idMng;
                            _cls._nombreTarifa = "Registered";
                            _cls._sku = _Texto[0];
                            _cls._precio = Convert.ToDecimal(_Texto[2], System.Globalization.CultureInfo.InvariantCulture);
                            //Lo sumo a la lista.
                            _lsPrecios.Add(_cls);
                        }
                    }

                    LogFile.ErrorLog(LogFile.CreatePath(), "El archivo se proceso correctamente.");

                    foreach (PreciosIcg _pr in _lsPrecios)
                    {
                        if (!String.IsNullOrEmpty(_pr._sku) || _pr._sku != "-")
                        {
                            if (_pr._nombreTarifa != "No Procesar")
                            {
                                try
                                {
                                    using (NopService.NopServiceClient _ws = new NopService.NopServiceClient())
                                    {
                                        _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(_urlNop);

                                        if (_pr._nombreTarifa == "Registered")
                                        {
                                            bool booRta = _ws.UpdatePrice(_userNop, _passNop, _pr._sku, _pr._precio, true);
                                            if (booRta)
                                                LogFile.ErrorLog(LogFile.CreatePath(), "Sincronizacion de Precio OK : " + _pr._nombreTarifa + ", " + _pr._sku);
                                            else
                                                LogFile.ErrorLog(LogFile.CreatePath(), "ERROR EN LA SINCRONIZACION de Precio: " + _pr._nombreTarifa + ", " + _pr._sku);
                                            
                                        }
                                        else
                                        {
                                            LogFile.ErrorLog(LogFile.CreatePath(), "No se sincroniza la tarifa: " + _pr._nombreTarifa);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string _body = "Se produjo el siguiente error: " + ex.Message + ". Para el articulo: " + _pr._sku + " y la lista: " + _pr._nombreTarifa + ". El proceso de Sincronizacion de Stock no termino correctamente. Por favor reviselo.";

                                    LogFile.ErrorLog(LogFile.CreatePath(), _body);
                                }
                            }
                            else
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(), "No Procesar por Rol Inexistente..");
                            }
                        }
                    }

                    LogFile.ErrorLog(LogFile.CreatePath(), "La Sincronizacion termino correctamente.");
                }
                catch (Exception ex)
                {
                    string _body = "Se produjo el siguiente error: " + ex.Message + ". El proceso de Sincronizacion de Stock no termino correctamente. Por favor reviselo.";

                    LogFile.ErrorLog(LogFile.CreatePath(), _body);

                    Email.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio, _asuntoEnvio, _body, true,
                        _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);
                }
            }
            else
            {
                LogFile.ErrorLog(LogFile.CreatePath(), "No existe el archivo: " + _archivoTxt);
            }
        }
    }
}
