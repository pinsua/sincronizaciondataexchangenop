﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SincronizacionPreciosVM
{
    class PreciosIcg
    {
        //Definicion MNG
        //1 = PUBLICO OFICIAL
        //2 = PUBLICO MASIVO
        //3 = FRANQUICIAS
        //4 = MAYORISTAS
        //5 = CLUB IAP
        //6 = CLUB PROFESIONAL
        //7 = COSTO OFICIALES
        //8 = CLUB IAP CURSO Y EDITORIAL


        public int _idTarifa { get; set; }
        public string _nombreTarifa { get; set; }
        public string _sku { get; set; }
        public decimal _precio { get; set; }
    }
}
