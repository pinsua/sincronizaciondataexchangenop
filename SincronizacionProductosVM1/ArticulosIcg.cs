﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace SincronizacionProductosVM1
{
    class ArticulosIcg
    {
        public string _sku { get; set; }
        public int _codigoArticulo { get; set; }
        public string _descatalogado { get; set; }
        public string _descripcion { get; set; }
        public string _refProveedor { get; set; }
        public DateTime _fechaModificado { get; set; }
        public bool _visibleWeb { get; set; }
        public decimal _peso { get; set; }
        public int _tipoImpuesto { get; set; }
        public decimal _ultimoCoste { get; set; }
        public string _departamento { get; set; }
        public string _seccion { get; set; }
        public string _familia { get; set; }
        public string _subfamilia { get; set; }
        public string _codBarras { get; set; }
        public string _codBarras2 { get; set; }
        public string _codBarras3 { get; set; }
        public string _talla { get; set; }
        public string _color { get; set; }
        public string _marca { get; set; }
        public string _linea { get; set; }

        public static List<ArticulosIcg> GetProducts(string _StrConnection)
        {
            List<ArticulosIcg> _lst = new List<ArticulosIcg>();

            string _sql = @"SELECT ARTICULOS.CODARTICULO, ARTICULOSLIN.CODBARRAS AS CODBARRAS, ARTICULOSLIN.CODBARRAS2 AS CODBARRAS2, ARTICULOSLIN.CODBARRAS3, DEPARTAMENTO.DESCRIPCION AS DEPARTAMENTO, FAMILIAS.DESCRIPCION AS FAMILIA, SECCIONES.DESCRIPCION AS SECCION, 
                        SUBFAMILIAS.DESCRIPCION AS SUBFAMILIA, ARTICULOS.DESCRIPCION, ARTICULOS.FECHAMODIFICADO, ARTICULOS.REFPROVEEDOR, ARTICULOS.VISIBLEWEB, ARTICULOSLIN.ULTIMOCOSTE, 
                        ARTICULOSLIN.DESCATALOGADO, ARTICULOSLIN.PESO, ARTICULOS.TIPOIMPUESTO, ARTICULOSLIN.TALLA, ARTICULOSLIN.COLOR,
						MARCA.DESCRIPCION AS MARCA, LINEA.DESCRIPCION AS LINEA
                        FROM            ARTICULOS INNER JOIN ARTICULOSLIN ON ARTICULOS.CODARTICULO = ARTICULOSLIN.CODARTICULO 
				        INNER JOIN SUBFAMILIAS ON ARTICULOS.DPTO = SUBFAMILIAS.NUMDPTO AND ARTICULOS.SECCION = SUBFAMILIAS.NUMSECCION 
					        AND ARTICULOS.FAMILIA = SUBFAMILIAS.NUMFAMILIA AND ARTICULOS.SUBFAMILIA = SUBFAMILIAS.NUMSUBFAMILIA 
				        INNER JOIN SECCIONES ON ARTICULOS.DPTO = SECCIONES.NUMDPTO AND ARTICULOS.SECCION = SECCIONES.NUMSECCION 
				        INNER JOIN FAMILIAS ON ARTICULOS.DPTO = FAMILIAS.NUMDPTO AND ARTICULOS.FAMILIA = FAMILIAS.NUMFAMILIA 
					        AND ARTICULOS.SECCION = FAMILIAS.NUMSECCION 
				        INNER JOIN DEPARTAMENTO ON ARTICULOS.DPTO = DEPARTAMENTO.NUMDPTO 
				        INNER JOIN PRECIOSVENTA ON ARTICULOSLIN.CODARTICULO = PRECIOSVENTA.CODARTICULO AND ARTICULOSLIN.TALLA = PRECIOSVENTA.TALLA AND ARTICULOSLIN.COLOR = PRECIOSVENTA.COLOR
				        INNER JOIN MARCA ON ARTICULOS.MARCA = MARCA.CODMARCA 
				        INNER JOIN LINEA ON ARTICULOS.MARCA = LINEA.CODMARCA AND ARTICULOS.LINEA = LINEA.CODLINEA
                        WHERE PRECIOSVENTA.IDTARIFAV = @Tarifa
                        ORDER BY CODARTICULO";

            try
            {
                using (SqlConnection _connection = new SqlConnection(_StrConnection))
                {
                    //Abrimos la conexion.
                    _connection.Open();
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.Connection = _connection;
                        _cmd.CommandText = _sql;
                        //Parametro
                        _cmd.Parameters.Add("@Tarifa", SqlDbType.Int).Value = 7;

                        using (SqlDataReader _reader = _cmd.ExecuteReader())
                        {
                            if (_reader.HasRows)
                            {
                                while (_reader.Read())
                                {
                                    ArticulosIcg _cls = new ArticulosIcg();
                                    _cls._sku = _reader["CODBARRAS"].ToString();
                                    _cls._codigoArticulo = Convert.ToInt32(_reader["CODARTICULO"]);
                                    _cls._descatalogado = _reader["DESCATALOGADO"].ToString();
                                    _cls._descripcion = _reader["DESCRIPCION"].ToString();
                                    _cls._refProveedor = _reader["REFPROVEEDOR"].ToString();
                                    _cls._fechaModificado = Convert.ToDateTime(_reader["FechaModificado"]);

                                    _cls._visibleWeb = _reader["VISIBLEWEB"].ToString().ToUpper() == "F" ? false : true;
                                    _cls._peso = Convert.ToDecimal(_reader["PESO"], System.Globalization.CultureInfo.InvariantCulture);
                                    _cls._tipoImpuesto = Convert.ToInt32(_reader["TIPOIMPUESTO"]);
                                    _cls._ultimoCoste = Convert.ToDecimal(_reader["ULTIMOCOSTE"], System.Globalization.CultureInfo.InvariantCulture);
                                    _cls._codBarras2 = _reader["CODBARRAS2"].ToString();
                                    _cls._codBarras3 = _reader["CODBARRAS3"].ToString();
                                    _cls._departamento = _reader["DEPARTAMENTO"].ToString();
                                    _cls._familia = _reader["FAMILIA"].ToString();
                                    _cls._seccion = _reader["SECCION"].ToString();
                                    _cls._subfamilia = _reader["SUBFAMILIA"].ToString();

                                    _cls._talla = _reader["TALLA"].ToString();
                                    _cls._color = _reader["COLOR"].ToString();
                                    _cls._marca = _reader["MARCA"].ToString();
                                    _cls._linea = _reader["LINEA"].ToString();

                                    _lst.Add(_cls);
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _lst;
        }
    }
}
