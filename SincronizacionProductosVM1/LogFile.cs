﻿using System;
using System.Collections.Generic;
using System.IO;


namespace SincronizacionProductosVM1
{
    class LogFile
    {
        /// <summary>
        /// Metodo que devuelve un string con el nombre del archivo mas la fecha
        /// </summary>
        /// <param name="pFileName">archivo</param>
        /// <returns></returns>
        public static string CreatePath()
        {
            string strFileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            return strFileName + DateTime.Now.ToShortDateString().Replace('/', '-') + ".log";
        }

        public static string KnowPath()
        {
            string strFileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
            return strFileName;
        }
        public static void ErrorLog(string pPathName, string pErrMsg)
        {
            string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";

            StreamWriter sw = new StreamWriter(pPathName, true);
            sw.WriteLine(sLogFormat + pErrMsg);
            sw.Flush();
            sw.Close();
        }
    }
}
