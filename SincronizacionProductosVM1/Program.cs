﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;
using System.Linq;

namespace SincronizacionProductosVM1
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Recupero los datos del Config
            string _archivoTxt = ConfigurationManager.AppSettings["ArchivoTxt"];
            string _userNop = ConfigurationManager.AppSettings["UserNop"];
            string _passNop = ConfigurationManager.AppSettings["PassNop"];
            string _urlNop = ConfigurationManager.AppSettings["UrlNop"];
            int _cantMinimaOrden = Convert.ToInt32(ConfigurationManager.AppSettings["CantMinimaOrden"]);
            int _cantMaximaOrden = Convert.ToInt32(ConfigurationManager.AppSettings["CantMaximaOrden"]);
            //Envio mail.
            string _destinatarioEmail = ConfigurationManager.AppSettings["DestinatarioEmail"];
            string _destinatarioCopia = ConfigurationManager.AppSettings["DestinatarioCopia"];
            string _direccionEnvio = ConfigurationManager.AppSettings["DireccionEnvio"];
            string _nicEnvio = ConfigurationManager.AppSettings["NicEnvio"];
            string _asuntoEnvio = ConfigurationManager.AppSettings["AsuntoEnvio"];
            string _passwordDireccionEnvio = ConfigurationManager.AppSettings["PasswordDireccionEnvio"];
            string _puertoSmtp = ConfigurationManager.AppSettings["PuertoSmtp"];
            string _servidorSmtp = ConfigurationManager.AppSettings["ServidorSmtp"];
            //StringConnection
            string _strConnecion = ConfigurationManager.ConnectionStrings["MngConnection"].ConnectionString;

            List<ArticulosIcg> _articulos = ArticulosIcg.GetProducts(_strConnecion);


            LogFile.ErrorLog(LogFile.CreatePath(), "El archivo se leyo correctamente.");

            //Obtengo las familias.
            List<string> _lstFamilia = _articulos.Select(x => x._familia).Distinct().ToList();
            //Obtengo las subfamilias.
            var _lstSubFamilia = _articulos.Select(x => new { x._familia, x._subfamilia }).Distinct().ToList();
            //Obtengo las marcas.
            List<string> _lstMarca = _articulos.Select(x => x._marca).Distinct().ToList();
            //Obtengo las lineas
            var _lstLineas = _articulos.Select(x => new { x._marca, x._linea }).Distinct().ToList();

        }
    }
}
