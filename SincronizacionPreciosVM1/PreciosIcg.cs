﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SincronizacionPreciosVM1
{
    class PreciosIcg
    {
        //Definicion MNG
        //1 = PUBLICO OFICIAL
        //2 = PUBLICO MASIVO
        //3 = FRANQUICIAS
        //4 = MAYORISTAS
        //5 = CLUB IAP
        //6 = CLUB PROFESIONAL
        //7 = COSTO OFICIALES
        //8 = CLUB IAP CURSO Y EDITORIAL


        public int _idTarifa { get; set; }
        public string _nombreTarifa { get; set; }
        public string _sku { get; set; }
        public decimal _precio { get; set; }

        public static List<PreciosIcg> GetPrices(string _StrConnection)
        {
            List<PreciosIcg> _lst = new List<PreciosIcg>();

            string _sql = @"SELECT  distinct      PRECIOSVENTA.IDTARIFAV, PRECIOSVENTA.PBRUTO, ARTICULOSLIN.CODBARRAS3
                        FROM          ARTICULOS inner join ARTICULOSLIN on ARTICULOS.CODARTICULO = ARTICULOSLIN.CODARTICULO
                          INNER JOIN PRECIOSVENTA ON ARTICULOSLIN.CODARTICULO = PRECIOSVENTA.CODARTICULO 
	                        AND ARTICULOSLIN.TALLA = PRECIOSVENTA.TALLA 
	                        AND ARTICULOSLIN.COLOR = PRECIOSVENTA.COLOR
                        WHERE PRECIOSVENTA.IDTARIFAV = @Tarifa";

            try
            {
                using (SqlConnection _connection = new SqlConnection(_StrConnection))
                {
                    //Abrimos la conexion.
                    _connection.Open();
                    using (SqlCommand _cmd = new SqlCommand())
                    {
                        _cmd.CommandType = System.Data.CommandType.Text;
                        _cmd.Connection = _connection;
                        _cmd.CommandText = _sql;
                        //Parametro
                        _cmd.Parameters.Add("@Tarifa", SqlDbType.Int).Value = 7;

                        using (SqlDataReader _reader = _cmd.ExecuteReader())
                        {
                            if (_reader.HasRows)
                            {
                                while (_reader.Read())
                                {
                                    PreciosIcg _cls = new PreciosIcg();
                                    _cls._sku = _reader["CODBARRAS3"].ToString();
                                    _cls._precio = Convert.ToDecimal(_reader["PBRUTO"]);
                                    _cls._idTarifa = Convert.ToInt16(_reader["IDTARIFA"]);
                                    
                                    _lst.Add(_cls);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return _lst;
        }
    }
}
