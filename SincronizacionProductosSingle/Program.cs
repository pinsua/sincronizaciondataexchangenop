﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;

namespace SincronizacionProductosSingle
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {

            string _archivoTxt = ConfigurationManager.AppSettings["ArchivoTxt"];
            string _userNop = ConfigurationManager.AppSettings["UserNop"];
            string _passNop = ConfigurationManager.AppSettings["PassNop"];
            string _urlNop = ConfigurationManager.AppSettings["UrlNop"];
            int _cantMinimaOrden = Convert.ToInt32(ConfigurationManager.AppSettings["CantMinimaOrden"]);
            int _cantMaximaOrden = Convert.ToInt32(ConfigurationManager.AppSettings["CantMaximaOrden"]);
            //Envio mail.
            string _destinatarioEmail = ConfigurationManager.AppSettings["DestinatarioEmail"];
            string _destinatarioCopia = ConfigurationManager.AppSettings["DestinatarioCopia"];
            string _direccionEnvio = ConfigurationManager.AppSettings["DireccionEnvio"];
            string _nicEnvio = ConfigurationManager.AppSettings["NicEnvio"];
            string _asuntoEnvio = ConfigurationManager.AppSettings["AsuntoEnvio"];
            string _passwordDireccionEnvio = ConfigurationManager.AppSettings["PasswordDireccionEnvio"];
            string _puertoSmtp = ConfigurationManager.AppSettings["PuertoSmtp"];
            string _servidorSmtp = ConfigurationManager.AppSettings["ServidorSmtp"];

            if (System.IO.File.Exists(_archivoTxt))
            {
                //Instancio la lista de articulos.
                List<ArticulosIcg> _lstArticulos = new List<ArticulosIcg>();
                try
                {
                    //Leo el archivo.
                    using (StreamReader sr = new StreamReader(_archivoTxt, false))
                    {
                        string line;
                        //ARTICULOS.CODARTICULO, ARTICULOS.DESCRIPCION, DEPARTAMENTO.DESCRIPCION AS DEPARTAMENTO, SECCIONES.DESCRIPCION AS SECCION, 
                        //FAMILIAS.DESCRIPCION AS FAMILIA, SUBFAMILIAS.DESCRIPCION AS SUBFAMILIA, ARTICULOS.REFPROVEEDOR, ARTICULOS.DESCATALOGADO, 
                        //ARTICULOS.VISIBLEWEB, ARTICULOSLIN.ULTIMOCOSTE, ARTICULOSLIN.CODBARRAS, ARTICULOSLIN.CODBARRAS2, ARTICULOSLIN.CODBARRAS3, 
                        //ARTICULOS.TIPOIMPUESTO, ARTICULOSLIN.PESO, ARTICULOS.FECHAMODIFICADO
                        while ((line = sr.ReadLine()) != null)
                        {
                            //separo todo por el ;
                            string[] _Texto = line.Split(';');
                            //fecha
                            LogFile.ErrorLog(LogFile.CreatePath(), "fecha" + _Texto[15]);
                            string _fecha = _Texto[15].Split('/')[1] + "/" + _Texto[15].Split('/')[0] + "/" + _Texto[15].Split('/')[2];
                            LogFile.ErrorLog(LogFile.CreatePath(), "fecha2" + _fecha);
                            LogFile.ErrorLog(LogFile.CreatePath(), "fecha3 " + DateTime.Now.Date.ToString());
                            //Instancio la clase y la cargo.
                            ArticulosIcg _cls = new ArticulosIcg();
                            _cls._sku = _Texto[10];
                            _cls._codigoArticulo = Convert.ToInt32(_Texto[0]);
                            _cls._descatalogado = _Texto[7];
                            _cls._descripcion = _Texto[1];
                            _cls._refProveedor = _Texto[6];
                            _cls._fechaModificado = Convert.ToDateTime(_Texto[15].ToString());
                            
                            _cls._visibleWeb = _Texto[8].ToString().ToUpper() == "F" ? false : true; 
                            _cls._peso = Convert.ToDecimal(_Texto[14], System.Globalization.CultureInfo.InvariantCulture);
                            _cls._tipoImpuesto = Convert.ToInt32(_Texto[13]);
                            _cls._ultimoCoste = Convert.ToDecimal(_Texto[9], System.Globalization.CultureInfo.InvariantCulture);
                            _cls._codBarras2 = _Texto[11];
                            _cls._codBarras3 = _Texto[12];
                            _cls._departamento = _Texto[2];
                            _cls._familia = _Texto[4];
                            _cls._seccion = _Texto[3];
                            _cls._subfamilia = _Texto[5];
                            //Lo sumo a la lista.
                            _lstArticulos.Add(_cls);
                        }
                    }

                    LogFile.ErrorLog(LogFile.CreatePath(), "El archivo se leyo correctamente.");

                    //Vemos si tenemos datos y los procesamos.
                    if (_lstArticulos.Count > 0)
                    {
                        foreach(ArticulosIcg _art in _lstArticulos)
                        {
                            if (!String.IsNullOrEmpty(_art._sku) && _art._codigoArticulo > 0 && _art._sku != "-")
                            {
                                using (NopService.NopServiceClient _ws = new NopService.NopServiceClient())
                                {
                                    _ws.Endpoint.Address = new System.ServiceModel.EndpointAddress(_urlNop);

                                    //Redondeamos el costo.
                                    decimal _costo = Math.Round(_art._ultimoCoste, 2);
                                    //Cargamos el Articulo.
                                    bool _rtaArticulo = _ws.NewSingleProduct(_userNop, _passNop, _art._sku, _art._descripcion, "", 
                                        _art._descripcion, _art._descripcion, _art._codigoArticulo.ToString(), _cantMinimaOrden, _cantMaximaOrden, "100", "90", 
                                        _costo.ToString(), _art._peso.ToString(), "0", "0", "0", _art._visibleWeb);
                                    //Comentamos todo por las dudas.
                                    //Analizar Rta.
                                    //if (_rtaArticulo)
                                    //{
                                    //    string _rtaPicture = _ws.AddProductPicture(_userNop, _passNop, _art._sku);
                                    //}
                                    //if (_rtaArticulo)
                                    //{
                                    //    if (!String.IsNullOrEmpty(_art._departamento))
                                    //    {
                                    //        string _rtaDepartamento = _ws.AddProductCategory(_userNop, _passNop, _art._departamento, _art._sku, "");
                                    //    }
                                    //    if (!String.IsNullOrEmpty(_art._seccion))
                                    //    {
                                    //        string _rtaSeccion = _ws.AddProductCategory(_userNop, _passNop, _art._seccion, _art._sku, _art._departamento);
                                    //    }
                                    //    if (!String.IsNullOrEmpty(_art._familia))
                                    //    {
                                    //        string _rtaFamilia = _ws.AddProductCategory(_userNop, _passNop, _art._familia, _art._sku,_art._seccion);
                                    //    }
                                    //    if (!String.IsNullOrEmpty(_art._subfamilia))
                                    //    {
                                    //        string _rtaSubFamilia = _ws.AddProductCategory(_userNop, _passNop, _art._subfamilia, _art._sku, _art._familia);
                                    //    }

                                    //}
                                }
                            }
                            else
                            {
                                LogFile.ErrorLog(LogFile.CreatePath(), "Revisar los datos de SKU y CodArticulo. Articulo: " + _art._sku);
                            }
                        }
                    }
                    else
                    {                        
                        LogFile.ErrorLog(LogFile.CreatePath(), "No hay datos a sincronizar.");
                    }
                }
                catch(Exception ex)
                {
                    string _body = "Se produjo el siguiente error: " + ex.Message + ". El proceso de Sincronizacion de Articulos no termino correctamente. Por favor reviselo.";

                    LogFile.ErrorLog(LogFile.CreatePath(), _body);

                    Email.Enviar("", _destinatarioEmail, _destinatarioCopia, _direccionEnvio, _nicEnvio, _asuntoEnvio, _body, true,
                        _passwordDireccionEnvio, Convert.ToInt32(_puertoSmtp), _servidorSmtp, true);
                }
            }

        }
    }
}
